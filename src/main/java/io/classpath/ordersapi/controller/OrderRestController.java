package io.classpath.ordersapi.controller;

import io.classpath.ordersapi.model.Order;
import io.classpath.ordersapi.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/orders")
@AllArgsConstructor
public class OrderRestController {

    private OrderService orderService;

    @GetMapping
    public Set<Order> fetchOrders(){
        return this.orderService.fetchOrders();
    }

    @GetMapping("/{id}")
    public Order fetchById(@PathVariable long id){
        return this.orderService.fetchOrderById(id);
    }

    @PostMapping
    public Order save(@RequestBody @Valid Order order){
        return this.orderService.save(order);
    }

    @DeleteMapping("/{id}")
    public void deleteOrderById(@PathVariable long id){
        this.orderService.deleteOrder(id);
    }
}