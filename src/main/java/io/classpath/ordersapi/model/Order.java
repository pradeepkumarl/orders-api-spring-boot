package io.classpath.ordersapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import static javax.persistence.GenerationType.AUTO;

@Entity
@Data
@EqualsAndHashCode(of = "id")
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = AUTO)
    private long id;

    @Min(value = 15000, message = "order price should be min of 15K")
    private double price;

    @NotNull
    private LocalDate orderDate;

    @NotEmpty
    private String customerName;
}