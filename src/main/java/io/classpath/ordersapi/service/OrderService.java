package io.classpath.ordersapi.service;

import io.classpath.ordersapi.model.Order;

import java.util.Set;

public interface OrderService {

    Order save(Order order);

    Set<Order> fetchOrders();

    Order fetchOrderById(long orderId);

    Order updateOrder(long orderId, Order order);

    void deleteOrder(long orderId);
}