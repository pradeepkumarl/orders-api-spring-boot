package io.classpath.ordersapi.service;

import io.classpath.ordersapi.model.Order;
import io.classpath.ordersapi.repository.OrderRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;

    @Override
    public Order save(Order order) {
        log.info(" Came inside the save method ",order );
        return this.orderRepository.save(order);
    }

    @Override
    public Set<Order> fetchOrders() {
        return new HashSet<>(this.orderRepository.findAll());
    }

    @Override
    public Order fetchOrderById(long orderId) {
        return this.orderRepository
                        .findById(orderId)
                        .orElseThrow(() -> new IllegalArgumentException("Invalid Order Id"));

    }

    @Override
    public Order updateOrder(long orderId, Order order) {
        this.orderRepository
                .findById(orderId)
                .ifPresent(savedOrder -> {
                    savedOrder.setOrderDate(order.getOrderDate());
                    savedOrder.setCustomerName(order.getCustomerName());
                    savedOrder.setId(order.getId());
                    savedOrder.setPrice(order.getPrice());
                    this.orderRepository.save(savedOrder);
                });
        return order;
    }

    @Override
    public void deleteOrder(long orderId) {
        this.orderRepository.deleteById(orderId);
    }
}