package io.classpath.ordersapi.exception;

import lombok.*;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
@Component
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Error> response(Exception exception){
        return ResponseEntity
                    .status(NOT_FOUND)
                    .body(Error.builder().errorCode(111).message(exception.getMessage()).build());
        // new User(112, 2, 23, "vinay", "kumar", "abccew", "ddsf");
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Error> invalidId(Exception exception){
        return ResponseEntity
                .status(BAD_REQUEST)
                .body(Error.builder().errorCode(111).message(exception.getMessage()).build());
        // new User(112, 2, 23, "vinay", "kumar", "abccew", "ddsf");
    }
}

@ToString
@AllArgsConstructor
@Builder
@Setter
@Getter
@NoArgsConstructor
class Error {

    private long errorCode;
    private String message;

}